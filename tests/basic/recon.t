#!/bin/bash

# Test *very basic* NSR functionality - startup, mount, simplest possible file
# write.

. $(dirname $0)/../include.rc
. $(dirname $0)/../volume.rc

function my_getfattr {
	getfattr --only-values -e text $* 2> /dev/null
}

function get_rep_count {
	v=$(my_getfattr -n trusted.nsr.rep-count $1)
	#echo $v > /dev/tty
	echo $v
}

function create_file {
	dd if=/dev/urandom of=$1 bs=4k count=$2 conv=sync 2> /dev/null
}

function kill_a_brick {
	for r in /var/lib/glusterd/vols/${V0}/run/*-recon.pid; do
		rpid=$(cat $r)
		echo "recon PID = $rpid" > /dev/tty
		b=$(echo $r | sed '/\(.*\):\(.*\)-recon.pid/s//\1\2.pid/')
		bpid=$(cat $b)
		echo "brick PID = $bpid" > /dev/tty
		kill -9 $bpid $rpid
		return 0
	done

	# No bricks?!?
	return 1
}

# Functions to check reconciliation for specific operation types.

function check_create_write {
	for b in $*; do
		cmp $tmpdir/create-write $b/create-write || return 1
	done
	return 0
}

function check_truncate {
	truncate --size=8192 $tmpdir/truncate
	for b in $*; do
		cmp $tmpdir/truncate $b/truncate || return 1
	done
	return 0
}

function check_hard_link {
	for b in $*; do
		inum1=$(ls -i $b/hard-link-1 | cut -d' ' -f1)
		inum2=$(ls -i $b/hard-link-2 | cut -d' ' -f1)
		[ "$inum1" = "$inum2" ] || return 1
	done
	return 0
}

function check_soft_link {
	for b in $*; do
		[ "$(readlink $b/soft-link)" = "soft-link-tgt" ] || return 1
	done
	return 0
}

function check_unlink {
	for b in $*; do
		[ ! -e $b/unlink ] || return 1
	done
	return 0
}

function check_mkdir {
	for b in $*; do
		[ -d $b/mkdir ] || return 1
	done
	return 0
}

function check_rmdir {
	for b in $*; do
		[ ! -e $b/rmdir ] || return 1
	done
}

function check_setxattr {
	for b in $*; do
		v=$(my_getfattr -n user.foo $b/setxattr)
		[ "$v" = "ash_nazg_durbatuluk" ] || return 1
	done
	return 0
}

function check_removexattr {
	for b in $*; do
		my_getfattr -n user.bar $b/removexattr 2> /dev/null
		[ $? = 0 ] && return 1
	done
	return 0
}

cleanup

TEST glusterd
TEST pidof glusterd
TEST $CLI volume info

TEST $CLI volume create $V0 replica 2 $H0:$B0/${V0}{1,2}

EXPECT "$V0" volinfo_field $V0 'Volume Name'
EXPECT 'Created' volinfo_field $V0 'Status'
EXPECT '2' brick_count $V0

TEST $CLI volume set $V0 cluster.nsr on
TEST $CLI volume set $V0 cluster.nsr.recon on

# This would normally be a terrible idea, but it's handy for issuing ops that
# will have to be reconciled later.
TEST $CLI volume set $V0 cluster.nsr.quorum-percent 0

TEST $CLI volume start $V0
EXPECT 'Started' volinfo_field $V0 'Status'

# Mount FUSE with caching disabled
TEST glusterfs --entry-timeout=0 --attribute-timeout=0 -s $H0 --volfile-id $V0 $M0

# Give the bricks a chance to connect to each other.
EXPECT_WITHIN 10 "2" get_rep_count $M0

# Create local files for comparisons etc.
tmpdir=$(mktemp -d)
trap "rm -rf $tmpdir" EXIT
TEST create_file $tmpdir/create-write 10
TEST create_file $tmpdir/truncate 10

# Prepare files and directories we'll need later.
TEST cp $tmpdir/truncate $M0/
TEST touch $M0/hard-link-1
TEST touch $M0/unlink
TEST mkdir $M0/rmdir
TEST touch $M0/setxattr
TEST touch $M0/removexattr
TEST setfattr -n user.bar -v "ash_nazg_gimbatul" $M0/removexattr

# Kill a brick and wait for a new leader to take over.
TEST kill_a_brick
sleep 10

# Test create+write
TEST cp $tmpdir/create-write $M0/
# Test truncate
TEST truncate --size=8192 $M0/truncate
# Test hard link
TEST ln $M0/hard-link-1 $M0/hard-link-2
# Test soft link
# Disabled here because it not only fails but crashes the recon daemon.
TEST ln -s soft-link-tgt $M0/soft-link
# Test unlink
TEST rm $M0/unlink
# Test mkdir
TEST mkdir $M0/mkdir
# Test rmdir
TEST rmdir $M0/rmdir
# Test setxattr
TEST setfattr -n user.foo -v "ash_nazg_durbatuluk" $M0/setxattr
# Test removexattr
TEST setfattr -x user.bar $M0/removexattr

# Restart the brick and give reconciliation a chance to run.
TEST $CLI volume start $V0 force
sleep 20

# Make sure everything is as it should be.
TEST check_create_write $B0/${V0}{1,2}
TEST check_truncate $B0/${V0}{1,2}
TEST check_hard_link $B0/${V0}{1,2}
TEST check_soft_link $B0/${V0}{1,2}
TEST check_unlink $B0/${V0}{1,2}
TEST check_mkdir $B0/${V0}{1,2}
TEST check_rmdir $B0/${V0}{1,2}
#EST check_setxattr $B0/${V0}{1,2}
#EST check_removexattr $B0/${V0}{1,2}

cleanup
#killall -9 etcd
