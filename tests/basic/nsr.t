#!/bin/bash

# Test *very basic* NSR functionality - startup, mount, simplest possible file
# write.

. $(dirname $0)/../include.rc
. $(dirname $0)/../volume.rc

function get_rep_count {
	v=$(getfattr --only-values -e text -n trusted.nsr.rep-count $1 2> /dev/null)
	#echo $v > /dev/tty
	echo $v
}

function ping_file {
	dd if=/dev/urandom of=$1 bs=4k count=100 2> /dev/null
}

cleanup

TEST glusterd
TEST pidof glusterd
TEST $CLI volume info

TEST $CLI volume create $V0 replica 2 $H0:$B0/${V0}{1,2}

EXPECT "$V0" volinfo_field $V0 'Volume Name'
EXPECT 'Created' volinfo_field $V0 'Status'
EXPECT '2' brick_count $V0

TEST $CLI volume set $V0 cluster.nsr on

TEST $CLI volume start $V0
EXPECT 'Started' volinfo_field $V0 'Status'

## Mount FUSE with caching disabled (read-only)
TEST glusterfs --entry-timeout=0 --attribute-timeout=0 -s $H0 --volfile-id $V0 $M0

# Give the bricks a chance to connect to each other.
EXPECT_WITHIN 10 "2" get_rep_count $M0

TEST ping_file $M0/probe
TEST cmp ${M0}/probe ${B0}/${V0}1/probe
TEST cmp ${M0}/probe ${B0}/${V0}2/probe

cleanup
killall -9 etcd
