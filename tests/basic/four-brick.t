#!/bin/bash

# Test *very basic* NSR functionality - startup, mount, simplest possible file
# write.

. $(dirname $0)/../include.rc
. $(dirname $0)/../volume.rc

function get_rep_count {
	v=$(getfattr --only-values -e text -n trusted.nsr.rep-count $1 2> /dev/null)
	#echo $v > /dev/tty
	echo $v
}

function ping_file {
	dd if=/dev/urandom of=$1 bs=4k count=100 2> /dev/null
}

function kill_brick {
	bpid=$(cat /var/lib/glusterd/vols/${V0}/run/*-${V0}${1}.pid)
	rpid=$(cat /var/lib/glusterd/vols/${V0}/run/*-${V0}${1}-recon.pid)
	echo "brick PID = $bpid" > /dev/tty
	echo "recon PID = $rpid" > /dev/tty
	kill -9 $bpid $rpid
}

function count_matches {
	n=0
	for f in $B0/$V0[1234]/$1; do
		cmp $M0/$1 $f 2> /dev/null
		if [ $? = 0 ]; then
			n=$((n+1))
		fi
	done
	echo $n
}

cleanup

TEST glusterd
TEST pidof glusterd
TEST $CLI volume info

TEST mkdir -p ${V0}{1,2,3,4}
TEST $CLI volume create $V0 replica 2 $H0:$B0/${V0}{1,2,3,4}

EXPECT "$V0" volinfo_field $V0 'Volume Name'
EXPECT 'Created' volinfo_field $V0 'Status'
EXPECT '4' brick_count $V0

TEST $CLI volume set $V0 cluster.nsr on
TEST $CLI volume set $V0 cluster.nsr.recon on

TEST $CLI volume start $V0
EXPECT 'Started' volinfo_field $V0 'Status'

## Mount FUSE with caching disabled (read-only)
TEST glusterfs --entry-timeout=0 --attribute-timeout=0 -s $H0 --volfile-id $V0 $M0

# Give the bricks a chance to connect to each other.
EXPECT_WITHIN 10 "2" get_rep_count $M0

TEST ping_file $M0/probe
TEST cmp ${M0}/probe ${B0}/${V0}1/probe
TEST cmp ${M0}/probe ${B0}/${V0}2/probe

# Kill one brick from each pair.
TEST kill_brick 1
TEST kill_brick 3
sleep 10

# Make sure only one copy makes it while degraded.
TEST ping_file $M0/probe2
TEST [ $(count_matches probe2) = 1 ]

# Restart the brick and give reconciliation a chance to run.
# TBD: figure out why reconciliation takes so $#@! long to run
TEST $CLI volume start $V0 force
sleep 20

# Make sure *both* copies are valid after reconciliation.
TEST [ $(count_matches probe2) = 2 ]

cleanup
#killall -9 etcd
