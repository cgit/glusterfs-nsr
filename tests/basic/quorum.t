#!/bin/bash

# Test *very basic* NSR functionality - startup, mount, simplest possible file
# write.

. $(dirname $0)/../include.rc
. $(dirname $0)/../volume.rc

function get_rep_count {
	v=$(getfattr --only-values -e text -n trusted.nsr.rep-count $1 2> /dev/null)
	#echo $v > /dev/tty
	echo $v
}

function kill_a_brick {
	for r in /var/lib/glusterd/vols/${V0}/run/*-recon.pid; do
		rpid=$(cat $r)
		#echo "recon PID = $rpid" > /dev/tty
		b=$(echo $r | sed '/\(.*\):\(.*\)-recon.pid/s//\1\2.pid/')
		bpid=$(cat $b)
		#echo "brick PID = $bpid" > /dev/tty
		kill -9 $bpid $rpid
		return 0
	done

	# No bricks?!?
	return 1
}

cleanup

TEST glusterd
TEST pidof glusterd
TEST $CLI volume info

TEST $CLI volume create $V0 replica 2 $H0:$B0/${V0}{1,2}

EXPECT "$V0" volinfo_field $V0 'Volume Name'
EXPECT 'Created' volinfo_field $V0 'Status'
EXPECT '2' brick_count $V0

TEST $CLI volume set $V0 cluster.nsr on
TEST $CLI volume set $V0 cluster.nsr.recon on

TEST $CLI volume start $V0
EXPECT 'Started' volinfo_field $V0 'Status'

## Mount FUSE with caching disabled (read-only)
TEST glusterfs --entry-timeout=0 --attribute-timeout=0 -s $H0 --volfile-id $V0 $M0

# Give the bricks a chance to connect to each other.
EXPECT_WITHIN 10 "2" get_rep_count $M0

TEST kill_a_brick
EXPECT_WITHIN 10 "1" get_rep_count $M0

# Make sure writes fail while degraded.
tmpfile=$(mktemp)
trap "rm $tmpfile" EXIT
dd if=/dev/urandom of=$M0/probe bs=4k count=100 status=none 2> $tmpfile
TEST [ x"$?" != x"0" ]
TEST grep -qs 'Read-only file system' $tmpfile

cleanup
