/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#include <sys/stat.h>
#include <sys/types.h>

#define LEADER_XATTR            "user.nsr.leader"
#define SECOND_CHILD(xl)        (xl->children->next->xlator)

enum {
        gf_mt_nsr_private_t = gf_common_mt_end + 1,
        gf_mt_nsr_fd_ctx_t,
        gf_mt_nsr_inode_ctx_t,
        gf_mt_nsr_dirty_t,
        gf_mt_nsr_end
};

typedef enum nsr_recon_notify_ev_id_t {
        NSR_RECON_SET_LEADER = 1,
        NSR_RECON_ADD_CHILD = 2
} nsr_recon_notify_ev_id_t;

typedef struct _nsr_recon_notify_ev_s {
        nsr_recon_notify_ev_id_t id;
        uint32_t index; // in case of add
        struct list_head list;
} nsr_recon_notify_ev_t;

typedef struct {
        char                    *etcd_servers;
        char                    *subvol_uuid;
        char                    *leader_key;
        char                    *term_key;
        char                    *brick_uuid;
        gf_boolean_t            leader;
        uint8_t                 up_children;
        uint8_t                 n_children;
        char                    *vol_file;
        etcd_session            etcd;
        volatile unsigned int   fence_io;
        uint32_t                current_term;
#ifdef  NSR_DEBUG
        uint32_t                leader_log_fd;
#endif
        volatile int            recon_notify_inited;
        volatile int            leader_inited;
        uint32_t                kid_state;
        gf_lock_t               dirty_lock;
        struct list_head        dirty_fds;
        gf_boolean_t            nsr_recon_start;
        void *                  recon_ctx;
        volatile uint32_t       ops_in_flight;
	uint32_t                index;
	gf_lock_t               index_lock;
        double                  quorum_pct;
} nsr_private_t;

typedef struct {
        call_stub_t             *stub;
        call_stub_t             *qstub;
        uint8_t                 call_count;
        fd_t                    *fd;
        struct list_head        qlinks;
} nsr_local_t;

/*
 * This should match whatever changelog returns on the pre-op for us to pass
 * when we're ready for our post-op.
 */
typedef uint32_t log_id_t;

typedef struct {
        struct list_head        links;
        log_id_t                id;
} nsr_dirty_list_t;

typedef struct {
        fd_t                    *fd;
        struct list_head        dirty_list;
        struct list_head        fd_list;
} nsr_fd_ctx_t;

typedef struct {
        gf_lock_t               lock;
        uint32_t                active;
        struct list_head        aqueue;
        uint32_t                pending;
        struct list_head        pqueue;
} nsr_inode_ctx_t;

void nsr_recon_notify_event_set_leader(nsr_private_t *priv);
void nsr_recon_notify_event_add_child(nsr_private_t *priv, uint32_t index);
void* nsr_recon_notify_thread (void *this);

