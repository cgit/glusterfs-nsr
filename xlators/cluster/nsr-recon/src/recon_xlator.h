/*
  Copyright (c) 2008-2012 Red Hat, Inc. <http://www.redhat.com>
  This file is part of GlusterFS.

  This file is licensed to you under your choice of the GNU Lesser
  General Public License, version 3 or any later version (LGPLv3 or
  later), or the GNU General Public License, version 2 (GPLv2), in all
  cases as published by the Free Software Foundation.
*/

#ifndef __RECON_XLATOR_H__
#define __RECON_XLATOR_H__

#include <semaphore.h>
#include <pthread.h>

enum gf_dht_mem_types_ {
        gf_mt_recon_changelog_buf_t = gf_common_mt_end + 1,
        gf_mt_recon_driver_ctx_t,
        gf_mt_recon_fd_t,
        gf_mt_recon_id_t,
        gf_mt_recon_member_name_t,
        gf_mt_recon_members_list_t,
        gf_mt_recon_per_node_worker_t,
        gf_mt_recon_private_t,
        gf_mt_recon_reconciliator_info_t,
        gf_mt_recon_record_t,
        gf_mt_recon_record_details_t,
        gf_mt_recon_role_work_t,
        gf_mt_recon_work_t,
        gf_mt_recon_work_data_t,
        gf_mt_recon_worker_t,
        gf_mt_recon_end,
};

enum nsr_recon_xlator_sector_t {
        nsr_recon_xlator_sector_0 = 0, // to report back the status of given transaction ids
        nsr_recon_xlator_sector_1 = 512, //  to write here information about leadership changes from the brick
        nsr_recon_xlator_sector_2 = (512 * 2), // to write here individual roles and wait for that role to be done
        nsr_recon_xlator_sector_3 = (512 *3), // read from here to get term info for given term
        nsr_recon_xlator_sector_4 = (512 * 4), // read from here to get last term info
};


typedef struct _nsr_recon_private_s {
        xlator_t *this; //back pointer
        unsigned int replica_group_size; // number of static members of replica group
        char **replica_group_members; // replica group members (including itself in first slot)
        pthread_t thread_id; // driver thread id
        nsr_recon_driver_ctx_t *driver_thread_context; //driver thread context
        unsigned int outstanding; // for communicating with driver thread
        call_frame_t *frame; // old frame that is pending (just one as of now)
        struct list_head list;
        char *volname;
        uint32_t txn_id;
        char *changelog_base_path;
        char *base_dir;
#if defined(NSR_DEBUG)
        FILE *fp;
#endif
} nsr_recon_private_t;

#define atomic_cmpxchg         __sync_val_compare_and_swap

#if defined(NSR_DEBUG)

extern void
_recon_main_log (const char *func, int line, char *member, FILE *fp,
                 char *fmt, ...);

#define recon_main_log(dom, levl, fmt...) do {                          \
        FMT_WARN (fmt);                                                 \
        if (levl <= nsr_debug_level) {                                  \
                nsr_recon_private_t     *priv   = this->private;        \
                _recon_main_log (__FUNCTION__, __LINE__,                \
                                 priv->replica_group_members[0],        \
                                 priv->fp,                              \
                                 ##fmt);                                \
        }                                                               \
} while (0)

#else
#define recon_main_log(dom, levl, fmt...) gf_log(dom, levl, fmt)
#endif

void nsr_recon_libchangelog_get_this_term_info(xlator_t *this, char *bp, int32_t term, nsr_recon_last_term_info_t *lt);
void nsr_recon_libchangelog_get_last_term_info(xlator_t *this, char *bp, int32_t term, nsr_recon_last_term_info_t *lt);
void nsr_recon_return_back(nsr_recon_private_t *priv, uint32_t term, int32_t status, int32_t op_errno);
gf_boolean_t nsr_recon_libchangelog_get_records(xlator_t *this, char *bp, int32_t term, uint32_t first, uint32_t last, void *buf);


#endif /* #ifndef __RECON_XLATOR_H__ */
