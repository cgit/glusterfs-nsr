/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#ifndef _CHANGELOG_FOPS_H
#define _CHANGELOG_FOPS_H

/* FOPS */

int32_t
changelog_default_rmdir (call_frame_t *frame, xlator_t *this,
                         loc_t *loc, int xflags, dict_t *xdata);
int32_t
changelog_default_unlink (call_frame_t *frame, xlator_t *this,
                          loc_t *loc, int xflags, dict_t *xdata);
int32_t
changelog_default_rename (call_frame_t *frame, xlator_t *this,
                          loc_t *oldloc, loc_t *newloc, dict_t *xdata);
int32_t
changelog_default_link (call_frame_t *frame,
                        xlator_t *this, loc_t *oldloc,
                        loc_t *newloc, dict_t *xdata);
int32_t
changelog_default_mkdir (call_frame_t *frame, xlator_t *this,
                         loc_t *loc, mode_t mode, mode_t umask, dict_t *xdata);
int32_t
changelog_default_symlink (call_frame_t *frame, xlator_t *this,
                           const char *linkname, loc_t *loc,
                           mode_t umask, dict_t *xdata);
int32_t
changelog_default_mknod (call_frame_t *frame,
                         xlator_t *this, loc_t *loc,
                         mode_t mode, dev_t dev, mode_t umask, dict_t *xdata);
int32_t
changelog_default_create (call_frame_t *frame, xlator_t *this,
                          loc_t *loc, int32_t flags, mode_t mode,
                          mode_t umask, fd_t *fd, dict_t *xdata);
int32_t
changelog_default_fsetattr (call_frame_t *frame,
                            xlator_t *this, fd_t *fd,
                            struct iatt *stbuf, int32_t valid, dict_t *xdata);
int32_t
changelog_default_setattr (call_frame_t *frame,
                           xlator_t *this, loc_t *loc,
                           struct iatt *stbuf, int32_t valid, dict_t *xdata);
int32_t
changelog_default_fremovexattr (call_frame_t *frame, xlator_t *this,
                                fd_t *fd, const char *name, dict_t *xdata);
int32_t
changelog_default_removexattr (call_frame_t *frame, xlator_t *this,
                               loc_t *loc, const char *name, dict_t *xdata);
int32_t
changelog_default_setxattr (call_frame_t *frame,
                            xlator_t *this, loc_t *loc,
                            dict_t *dict, int32_t flags, dict_t *xdata);
int32_t
changelog_default_fsetxattr (call_frame_t *frame,
                             xlator_t *this, fd_t *fd, dict_t *dict,
                             int32_t flags, dict_t *xdata);
int32_t
changelog_default_truncate (call_frame_t *frame,
                            xlator_t *this, loc_t *loc,
                            off_t offset, dict_t *xdata);
int32_t
changelog_default_ftruncate (call_frame_t *frame,
                             xlator_t *this, fd_t *fd,
                             off_t offset, dict_t *xdata);
int32_t
changelog_default_writev (call_frame_t *frame,
                          xlator_t *this, fd_t *fd, struct iovec *vector,
                          int32_t count, off_t offset, uint32_t flags,
                          struct iobref *iobref, dict_t *xdata);

/* COPS */
int
changelog_default_cops_open (xlator_t *, changelog_priv_t *,
                             void *, char*, gf_boolean_t);

int
changelog_default_cops_close (xlator_t *, changelog_priv_t *, void *);

int
changelog_default_cops_sync (xlator_t *this,
                             changelog_priv_t *priv, void *);

int
changelog_default_cops_rollover (xlator_t *,
                                 changelog_priv_t *, void *,
                                 char *, gf_boolean_t);
int
changelog_default_cops_write (xlator_t *,
                              changelog_priv_t *, void *,
                              changelog_local_t *, changelog_log_type);

int
changelog_default_cops_read (xlator_t *,
                             changelog_priv_t *, void *, char *);

int
changelog_default_cops_unlink (xlator_t *,
                               changelog_priv_t *, void *, char *);

off_t
changelog_default_cops_get_offset (xlator_t *,
                                   changelog_priv_t *, void *,
                                   changelog_local_t *);

void
changelog_default_cops_set_offset (xlator_t *,
                                   changelog_priv_t *, void *,
                                   changelog_local_t *, off_t );

void
changelog_default_cops_reset_offset (xlator_t *, changelog_priv_t *,
                                     void *, changelog_local_t *);


GF_UNUSED static struct xlator_fops changelog_default_fops = {
        .mknod        = changelog_default_mknod,
        .mkdir        = changelog_default_mkdir,
        .create       = changelog_default_create,
        .symlink      = changelog_default_symlink,
        .writev       = changelog_default_writev,
        .truncate     = changelog_default_truncate,
        .ftruncate    = changelog_default_ftruncate,
        .link         = changelog_default_link,
        .rename       = changelog_default_rename,
        .unlink       = changelog_default_unlink,
        .rmdir        = changelog_default_rmdir,
        .setattr      = changelog_default_setattr,
        .fsetattr     = changelog_default_fsetattr,
        .setxattr     = changelog_default_setxattr,
        .fsetxattr    = changelog_default_fsetxattr,
        .removexattr  = changelog_default_removexattr,
        .fremovexattr = changelog_default_fremovexattr,
};

GF_UNUSED static struct changelog_ops changelog_default_cops = {
        .open         = changelog_default_cops_open,
        .sync         = changelog_default_cops_sync,
        .read         = changelog_default_cops_read,
        .close        = changelog_default_cops_close,
        .write        = changelog_default_cops_write,
        .unlink       = changelog_default_cops_unlink,
        .rollover     = changelog_default_cops_rollover,
        .get_offset   = changelog_default_cops_get_offset,
        .set_offset   = changelog_default_cops_set_offset,
        .reset_offset = changelog_default_cops_reset_offset,
};

#endif /* _CHANGELOG_FOPS_H */
