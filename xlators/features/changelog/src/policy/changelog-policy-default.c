/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#include "changelog-policy.h"
#include "changelog-fops.h"

int
changelog_default_policy_init (xlator_t *this,
                               changelog_priv_t *priv,
                               struct changelog_logpolicy *cp)
{
        priv->rollover_time = 15;

        priv->no_gfid_hdr = _gf_false;
        priv->lockless_update = _gf_false;

        cp->cpriv = GF_CALLOC (1, sizeof (off_t),
                                 gf_changelog_mt_fop_policy_t);
        if (!cp->cpriv)
                return -1;

        (void) memset (cp->changelog_name, '\0', PATH_MAX);
        (void) memcpy (cp->changelog_name,
                       CHANGELOG_FILE_NAME, strlen (CHANGELOG_FILE_NAME));

        cp->fops = &changelog_default_fops; /* default logging policy */
        cp->cops = &changelog_default_cops; /* default changelog operations */

        return 0;
}

int
changelog_default_policy_fini (xlator_t *this,
                               struct changelog_logpolicy *cp)
{
        GF_FREE (cp->cpriv);
        return 0;
}
