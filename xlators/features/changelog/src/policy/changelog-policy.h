/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#ifndef _CHANGELOG_POLICY_H
#define _CHANGELOG_POLICY_H

#ifndef _CONFIG_H
#define _CONFIG_H
#include "config.h"
#endif

#include "xlator.h"
#include "defaults.h"
#include "logging.h"

#include "changelog-mem-types.h"
#include "changelog-helpers.h"

int
changelog_default_policy_init (xlator_t *this,
                               changelog_priv_t *priv,
                               struct changelog_logpolicy *);
int
changelog_default_policy_fini (xlator_t *this,
                               struct changelog_logpolicy *);
int
changelog_replication_policy_init (xlator_t *this,
                                   changelog_priv_t *priv,
                                   struct changelog_logpolicy *cp);
int
changelog_replication_policy_fini (xlator_t *this,
                                   struct changelog_logpolicy *cp);

#endif /* _CHANGELOG_POLICY_H */
