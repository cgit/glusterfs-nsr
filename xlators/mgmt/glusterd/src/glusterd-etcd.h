/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#ifndef _GLUSTERD_ETCD_H_
#define _GLUSTERD_ETCD_H_

#include <sys/types.h>
#include "glusterfs.h"

pid_t   start_etcd      (char *this_host, char *other_host);

void    stop_etcd       (pid_t pid);

void    nuke_etcd_dir   (void);

#endif
