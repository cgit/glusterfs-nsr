/*
   Copyright (c) 2013 Red Hat, Inc. <http://www.redhat.com>
   This file is part of GlusterFS.

   This file is licensed to you under your choice of the GNU Lesser
   General Public License, version 3 or any later version (LGPLv3 or
   later), or the GNU General Public License, version 2 (GPLv2), in all
   cases as published by the Free Software Foundation.
*/

#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "glusterfs.h"
#include "run.h"
#include "glusterd-etcd.h"

#define GLUSTERD_ETCD_DIR       "/var/lib/glusterd/etcd"
#define GLUSTERD_ETCD_CMD       "/root/etcd/bin/etcd"

pid_t
start_etcd (char *this_host, char *other_host)
{
        runner_t        runner;
        char            me[256];

        if (gethostname(me,sizeof(me)-1) != 0) {
                gf_log (__func__, GF_LOG_ERROR, "gethostname failed?!?");
                return -1;
        }
        me[sizeof(me)-1] = '\0';

        if ((mkdir(GLUSTERD_ETCD_DIR,0700) < 0) && (errno != EEXIST)) {
                gf_log (__func__, GF_LOG_ERROR,
                        "failed to create %s", GLUSTERD_ETCD_DIR);
                return -1;
        }

        runinit (&runner);
        runner_add_args (&runner, GLUSTERD_ETCD_CMD,
                                  "-name", this_host,
                                  "-data-dir", GLUSTERD_ETCD_DIR,
                                  "-bind-addr", NULL);
        runner_argprintf( &runner, "%s:4001", me);
        runner_add_arg (&runner, "-peer-addr");
        runner_argprintf (&runner, "%s:7001", me);
        if (other_host) {
                runner_add_arg (&runner, "-peers");
                runner_argprintf (&runner, "%s:7001", other_host);
                gf_log (__func__, GF_LOG_INFO, "starting etcd  via %s", other_host);
        } else {
                gf_log (__func__, GF_LOG_INFO, "starting etcd standalone");
        }

        /*
         * Runner_run would wait for it.  Runner_run_nowait would not wait,
         * but would detach it so thoroughly that it won't die when we do.
         * Also, runner->chpid would be the PID of the transient middle
         * process, not the one we might actually need to kill later.  This
         * seems to do exactly what we need.
         */
        if (runner_start(&runner) != 0) {
                gf_log (__func__, GF_LOG_ERROR,
                        "failed to start %s", GLUSTERD_ETCD_CMD);
                return -1;
        }

        return runner.chpid;
}

void
stop_etcd (pid_t pid)
{
        if (pid > 0) {
                gf_log (__func__, GF_LOG_INFO, "killing etcd %d", pid);
                (void)kill(pid,SIGKILL);
                (void)waitpid(pid,NULL,0);
        }
}

void
nuke_etcd_dir (void)
{
        (void)runcmd("rm","-rf",GLUSTERD_ETCD_DIR,NULL);
}
